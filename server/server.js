/*var express        =        require("express");
var bodyParser     =        require("body-parser");
var app            =        express();*/


var app = require('http').createServer()
var io = require('socket.io')(app);
var request = require('request');

var usuarios_conectados = [];
var ruta = 'http://127.0.0.1:8888/data/logica';

app.listen(3001,"127.0.0.1");
console.log('Server running at http://127.0.0.1:3001/');

io.on('connection', function (socket) {
  
	socket.emit('connected', { connected: true , con_id: socket.id});

	//Registrar usuario al notificador
	socket.on("reguser",function(data){
		if(typeof data.id_usuario === "undefined"){
	        io.to(socket.id).emit('disconnect', {"authFail" : true, "title" : "Lo sentimos la sesión no es valida!"});
	        socket.disconnect();
	        return false;
		}
		addUsuarioConectado(data.id_usuario,socket.id);
      	io.sockets.emit('listusers', { test : 'Angelica' });
	});

	socket.on('enviarNotify', function (data) {
	  	//consultar a quien se le va a enviar la notificacion
	  	//var id_usuario = ['1234']; 
	  	//NOTA: FALTA ELIMINAR AL USUARIO DEL ARREGLO USUARIOS_CONECTADOS CUANDO SE DESCONECTE DEL SOCKET
	  	request({
		 	method: 'post',
		  	url: ruta+'notify.php?accion=devolverDatoNotify&estado=0&key=serverNode123456&tipo_accion='+data.tipo_accion+'&user='+data.user,
		  	json: true,
		}, function (err, res, body) {
			if (err) {
				return false;
			}

			console.log(body);

		  	/*if(body.error != 1){
				var datos_recibidos = body.data_mensaje;

		  		if(body.tipo == "todos"){
		  			console.log("Entro a if");
		  			//var dato_enviar = { "id" : '', "mensaje" : mensaje };
		            io.sockets.emit('recibirNotify', datos_recibidos);
		            console.log("Enviando notifiacion a todos los usuarios conectados.");
		  		}else{
		  			console.log("Entro a else");
		  			for (var i = 0; i < datos_recibidos.length; i++) {
			  			var receiver = datos_recibidos[i].id;
			  			if(typeof usuarios_conectados["C_"+receiver] !== "undefined"){
			                var id_conn = usuarios_conectados["C_"+receiver].id;
			                console.log(datos_recibidos[i]);
			                io.to(id_conn).emit('recibirNotify', datos_recibidos);
			            }
				    }
		  		}

		  	}*/

		});
  	});

});

function addUsuarioConectado(id_user_conexion,id_conexion){
    if(typeof usuarios_conectados["C_"+id_user_conexion] === "undefined"){
        usuarios_conectados["C_"+id_user_conexion] = {"dniper" : id_user_conexion, "id" : id_conexion};
        console.log("Usuario Agregado "+ id_user_conexion);
    }else{
        usuarios_conectados["C_"+id_user_conexion].id = id_conexion;
        console.log("Usuario actualizado id conexion " + id_user_conexion);
    }
}

